# Wireshark dissector for FitPro protocol

Incomplete but proof of concept dissector for the FitPro protocol. Place it into `~/.local/lib/wireshark/plugins` . Apply this by typing FitPro into the `display filter` top input line.

# Wireshark color scheme for FitPro protocol

Very simple highligh of some special packets of the protocol, to be used with the above mentioned dissector. Import in into Wireshark via menu → `View` → `Colorizing Rules...` → `Import`. Apply via menu → `View` → `Colorize Packet List`.
