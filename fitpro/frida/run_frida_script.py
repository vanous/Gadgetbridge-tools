import frida, sys

script_path=sys.argv[1]

def test_function():
    printt("somethins")


def on_message(message, data):
    if message['type'] == 'send':
        print("[*] {0}".format(message['payload']))
    else:
        print(message)

process = frida.get_usb_device().attach("FitPro")
script = process.create_script(open(script_path).read())
script.on('message', on_message)
script.load()

# Prevent the script from terminating
input()
